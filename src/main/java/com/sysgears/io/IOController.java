package com.sysgears.io;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Communicates with user via OutputStream and InputStream.
 */
public class IOController implements IReaderWriter {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(IOController.class);

    /**
     * Stores instance of OutputStream to receive messages.
     */
    private final OutputStream outputStream;

    /**
     * Stores instance of InputStreams to send messages.
     */
    private final InputStream inputStream;

    /**
     * Creates an instance with given OutputStream and InputStream sources to send and receive messages.
     *
     * @param outputStream source in which messages will be sent
     * @param inputStream  the source from which it will receive messages
     */
    public IOController(final OutputStream outputStream, final InputStream inputStream) {
        this.outputStream = outputStream;
        this.inputStream = inputStream;
        log.debug("Initialising IOController.");
    }

    /**
     * Reads message from current source.
     *
     * @return message from received from source
     * @throws IOException if exception during read was thrown
     */
    public String read() throws IOException {
        byte[] bytes = new byte[255];
        inputStream.read(bytes);
        String str = null;
        for (int i = 0; i < bytes.length; i++) {
            if (bytes[i] == 0) {
                str = new String(bytes, 0, --i);
                break;
            }
        }

        return str;
    }

    /**
     * Sends message to output source.
     *
     * @param message data message with result of application run
     * @throws IOException if exception during write process was thrown
     */
    public void write(final String message) throws IOException {
        outputStream.write(message.getBytes());
    }
}