package com.sysgears.fileworker;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Works with files.
 */
public class FileWorker {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(FileWorker.class);

    /**
     * Creates new FileWorker instance.
     */
    public FileWorker() {
        log.debug("Initialising FileWorker");
    }

    /**
     * Creates list of files from paths.
     *
     * @param paths which contains array of files path
     * @return array of files for command
     * @throws FileNotFoundException if file at specified path wasn't found
     */
    public List<File> getFiles(final String[] paths) throws FileNotFoundException {
        log.info("Starting FileWorker getFiles method. With parameter: " + paths);
        if (paths == null) {
            throw new IllegalArgumentException("Any files given!");
        }
        List<File> files = new ArrayList<File>();
        for (String path : paths) {
            File file = new File(path);
            if (!file.exists()) {
                log.warn("File at path: " + path + " wasn't found.");
                throw new FileNotFoundException("File at path: " + path + " wasn't found.");
            }
            files.add(file);
        }
        log.info("All files exist.");

        return files;
    }

    /**
     * Replace all given sequence in file with new sequence.
     *
     * @param file        on which to replace sequence
     * @param oldSequence old sequence which need to be removed
     * @param newSequence new sequence to replace old one
     * @return status of operation
     * @throws IOException if I\O exception occurs
     */
    public String rewriteSequenceInFile(final File file,
                                        final String oldSequence,
                                        final String newSequence) throws IOException {
        String answer = null;
        File newFile = getNewTemporaryFile(file.getAbsolutePath());
        FileRW fileRW = new FileRW(file);
        int linesNumber = 0;
        String oldLine = fileRW.getNextLine();
        while (oldLine != null) {
            String newLine = oldLine.replaceAll(oldSequence, newSequence);
            fileRW.writeLineToFile(newLine);
            if (!oldLine.equals(newLine)) {
                linesNumber++;
            }
            oldLine = fileRW.getNextLine();
        }
        if (renameAndDeleteOld(newFile, file)) {
            answer = "All passed normally. Sequence was replaced in " + linesNumber + " lines.";
        }
        return answer;
    }

    /**
     * Renames first file at name of second. After that removes second file.
     *
     * @param newFile which need to be renamed
     * @param oldFile which need to be removed at the end
     * @return true if everything pass normally or IOException
     * @throws IOException if files cannot be removed or renamed
     */
    public boolean renameAndDeleteOld(final File newFile, final File oldFile) throws IOException {
        if (oldFile.delete()) {
            if (!newFile.renameTo(oldFile)) {
                throw new IOException("Cannot rename files.");
            }
        }

        return true;
    }

    /**
     * Creates new temporary file with given path.
     *
     * @param path for new temporary file
     * @return new temporary file or IOException
     * @throws IOException if file at that path cannot be created
     */
    private File getNewTemporaryFile(final String path) throws IOException {
        File tmpFile = new File(path + " tmp");
        if (!tmpFile.createNewFile()) {
            throw new IllegalArgumentException("File - \"" + path + " tmp\" already exist!");
        }

        return tmpFile;
    }

}
