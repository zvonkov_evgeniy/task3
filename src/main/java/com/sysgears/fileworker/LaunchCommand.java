package com.sysgears.fileworker;

import com.sysgears.filecommands.CommandHolder;
import com.sysgears.filecommands.IFileCommand;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Launch command with given parameters.
 */
public class LaunchCommand {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(LaunchCommand.class);

    /**
     * Stores instance of CommandHolder.
     */
    private final CommandHolder commandHolder;

    /**
     * Stores instance of FileWorker.
     */
    private final FileWorker fileWorker;

    /**
     * Creates new instance of CommandArguments.
     *
     * @param commandHolder instance of CommandHolder
     * @param fileWorker    instance of FileWorker
     */
    public LaunchCommand(final CommandHolder commandHolder, final FileWorker fileWorker) {
        this.commandHolder = commandHolder;
        this.fileWorker = fileWorker;
        log.debug("Initialise LaunchCommand.");
    }

    /**
     * Returns parsed arguments for command from input data.
     *
     * @param command  command with options
     * @param fileArgs contains not parsed arguments
     * @return parsed arguments
     * @throws ParseException if exception during parsing occurs
     */
    public CommandLine getCommandArguments(final IFileCommand command, final String[] fileArgs) throws ParseException {
        log.info("Starting to parse arguments.");
        CommandLineParser parser = new PosixParser();
        CommandLine commandLine = parser.parse(command.getOptions(), fileArgs, false);
        log.info("All arguments successfully parsed.");
        return commandLine;
    }

    /**
     * Executes command for giving arguments.
     *
     * @param expression contains command and her arguments
     * @return array of string with result statuses
     * @throws IOException    if I\O exception occurs
     * @throws ParseException if exception during parsing occurs
     */
    public String[] execute(final String expression) throws IOException, ParseException {
        log.info("Starting LaunchCommand execute method.");
        log.info("Getting command.");
        String commandName = expression.split(" ")[0];
        IFileCommand command = commandHolder.getCommandByName(commandName);
        log.info("Getting arguments.");
        CommandLine commandLine = getCommandArguments(command, expression.split(commandName)[1].split(" "));
        log.info("Getting files.");
        ArrayList<File> files = (ArrayList<File>) fileWorker.getFiles(commandLine.getOptionValues("f"));
        String[] executionResult = new String[files.size()];
        for (int i = 0; i < files.size(); i++) {
            log.info("Executing command.");
            executionResult[i] = command.make(files.get(i), commandLine);
        }
        log.info("Successfully passed execute method.");

        return executionResult;
    }
}