package com.sysgears.fileworker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 */
public class FileRW {

    private final Scanner reader;

    private final FileWriter writer;

    public FileRW(final File file) throws IOException {
        reader = new Scanner(file);
        writer = new FileWriter(file);
    }

    public String getNextLine() {
        String nextLine = null;
        if (reader.hasNextLine()) {
            nextLine = reader.nextLine();
        } else {
            reader.close();
        }
        return nextLine;
    }

    public void writeLineToFile(final String line) throws IOException {
        boolean writerMustBeClosed = false;
        try {
            writer.write(line);
        } catch (IOException ioe) {
            writerMustBeClosed = true;
        }
        if (writerMustBeClosed) {
            writer.close();
        }
    }
}
