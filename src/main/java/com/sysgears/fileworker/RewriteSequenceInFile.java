package com.sysgears.fileworker;

import java.io.File;

/**
 */
public class RewriteSequenceInFile {

    private final File file;

    private final String oldSequence;

    private final String newSequence;


    public RewriteSequenceInFile(final File file, final String oldSequence, final String newSequence) {
        this.file = file;
        this.oldSequence = oldSequence;
        this.newSequence = newSequence;
    }


}
