package com.sysgears.service;

/**
 * Stores command for application.
 */
public enum ApplicationCommands {

    /**
     * Stops the application.
     */
    EXIT("exit"),

    /**
     * Stores info about application usage.
     */
    HELP("help"),

    /**
     * Signals that command is unknown.
     */
    DEFAULT("unknown");

    /**
     * Stores command name.
     */
    private final String name;

    /**
     * Creates instance of Command with specified name.
     *
     * @param name of the command
     */
    ApplicationCommands(final String name) {
        this.name = name;
    }

    /**
     * Returns string which contains name of the current command.
     *
     * @return command name
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Returns command corresponding to the given name.
     *
     * @param name string which contains command name
     * @return command corresponding to given name
     */
    public static ApplicationCommands findByName(final String name) {
        ApplicationCommands command = DEFAULT;
        for (ApplicationCommands commands : values()) {
            if (commands.name.equals(name.toLowerCase())) {
                command = commands;
            }
        }

        return command;
    }
}
