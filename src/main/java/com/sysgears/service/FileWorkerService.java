package com.sysgears.service;

import com.sysgears.filecommands.CommandHolder;
import com.sysgears.fileworker.LaunchCommand;
import com.sysgears.io.IReaderWriter;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Starts application and control it.
 */
public class FileWorkerService {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(FileWorkerService.class);

    /**
     * Stores source for sending and getting messages.
     */
    private final IReaderWriter ioSource;

    /**
     * Stores instance of LaunchCommand for run commands on files.
     */
    private final LaunchCommand launchCommand;

    /**
     * Stores instance of CommandFactory.
     */
    private final CommandHolder commandHolder;

    /**
     * Creates new instance of FileWorkerService.
     *
     * @param ioSource      for communicate with user
     * @param launchCommand for running commands on files
     * @param commandHolder command holder instances
     */
    public FileWorkerService(final IReaderWriter ioSource,
                             final LaunchCommand launchCommand,
                             final CommandHolder commandHolder) {
        this.ioSource = ioSource;
        this.launchCommand = launchCommand;
        this.commandHolder = commandHolder;
        log.debug("Initialise FileWorkerService.");
    }

    /**
     * Starts application. Gets command from source and pass them to FileWorker.
     *
     * @throws IOException if io exception occurs
     */
    public void service() throws IOException {
        log.info("Starting service method.");
        String command;
        boolean exit = false;
        while (!exit) {
            ioSource.write("Type your command and press <Enter>" + "\n");
            command = ioSource.read();
            log.info("Getting command: " + command);
            ioSource.write("You type: " + command + "\n\n");
            if (ApplicationCommands.findByName(command) != null) {
                switch (ApplicationCommands.findByName(command)) {
                    case HELP:
                        for (int i = 0; i < commandHolder.getCommands().size(); i++) {
                            ioSource.write(commandHolder.getCommands().get(i).getHelp());
                        }
                        break;
                    case EXIT:
                        ioSource.write("Work is done.");
                        exit = true;
                        break;
                    case DEFAULT:
                        try {
                            String[] results = launchCommand.execute(command);
                            for (String result : results) {
                                ioSource.write(result + "\n");
                            }
                        } catch (FileNotFoundException not) {
                            log.warn("FileNotFoundException", not);
                            ioSource.write(not.getMessage() + "\n");
                        } catch (IllegalArgumentException iae) {
                            log.warn("IllegalArgumentException", iae);
                            ioSource.write(iae.getMessage() + "\n");
                        } catch (ParseException pe) {
                            log.warn("ParseException", pe);
                            ioSource.write(pe.getMessage() + " option wasn't specified" + "\n");
                        }
                }
            }
            log.info("Making another iteration.");
        }
    }
}
