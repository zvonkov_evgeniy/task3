package com.sysgears.filecommands;

import com.sysgears.fileworker.FileWorker;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;

/**
 * Replace all duplicates of sequence in file.
 */
public class ReplaceSequence implements IFileCommand {

    /**
     * Stores options for this command.
     */
    private final OptionsHolder options;

    /**
     * Stores instance of FileWorker, which works with files.
     */
    private final FileWorker fileWorker;

    /**
     * Creates new instance of ReplaceSequence.
     *
     * @param options    instance of CommandOptions
     * @param fileWorker instance of FileWorker
     */
    public ReplaceSequence(final OptionsHolder options, final FileWorker fileWorker) {
        this.fileWorker = fileWorker;
        this.options = options;
        setOptions();
    }

    /**
     * Adds options for command in CommandOptions instance.
     */
    private void setOptions() {
        options.addOption("s", true, "Sequence to replace.", 1, "sequence", true);
        options.addOption("f", true, "Files on what to execute command", 10, "files", true);
        options.addOption("n", true, "New sequence.", 1, "new sequence", true);
    }

    /**
     * Returns this command name for run.
     *
     * @return replace
     */
    @Override
    public String getName() {
        return "replace";
    }

    /**
     * Returns options for this command.
     *
     * @return options for this command
     */
    @Override
    public Options getOptions() {
        return options.getOptions();
    }

    /**
     * Replace all given sequence in file with new sequence.
     *
     * @param file        on which run command
     * @param commandLine arguments for command
     * @return status of operation
     * @throws IOException if I\O exception occurs
     */
    @Override
    public String make(final File file, final CommandLine commandLine) throws IOException {
        String sequence = commandLine.getOptionValue("s");
        String newSequence = commandLine.getOptionValue("n");
        fileWorker.rewriteSequenceInFile(file, sequence, newSequence);

        return fileWorker.rewriteSequenceInFile(file, sequence, newSequence);
    }

    /**
     * Returns information of this class as string.
     *
     * @return information of class
     */
    @Override
    public String getHelp() {
        String answer = "Replace all sequence in files with new one. Usage:\n" + "replace -option -argument:\n";

        return (answer + options.getHelp());
    }
}
