package com.sysgears.filecommands;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Returns command that implements IFileCommand;
 */
public class CommandHolder {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(CommandHolder.class);

    /**
     * Stores list of current exist commands.
     */
    private final ArrayList<IFileCommand> commands = new ArrayList<IFileCommand>();

    /**
     * Creates Command Factory.
     */
    public CommandHolder() {
        log.debug("Initialising CommandFactory");
    }

    /**
     * Returns current command.
     *
     * @param command name of command
     * @return current command
     */
    public IFileCommand getCommandByName(final String command) {
        log.info("Starting CommandFactory getCommandByName method. With parameter: " + command);
        IFileCommand currentCommand = null;
        for (IFileCommand fileCommand : commands) {
            if (fileCommand.getName().equals(command)) {
                currentCommand = fileCommand;
                break;
            }
        }
        if (currentCommand == null) {
            log.warn("Unsupported command.");
            throw new IllegalArgumentException("Can't recognize this command.");
        }
        log.info("Command obtained successfully.");
        return currentCommand;
    }

    /**
     * Returns current command list.
     *
     * @return current stored commands list
     */
    public List<IFileCommand> getCommands() {
        log.info("Starting CommandHolder getCommands method.");
        //noinspection unchecked
        return (ArrayList<IFileCommand>) commands.clone();
    }

    /**
     * Adds command to holder.
     *
     * @param command instance of command to add to holder
     */
    public void addCommandToHolder(final IFileCommand command) {
        commands.add(command);
        log.debug("Instance of IFileCommand was successfully added.");
    }
}
