package com.sysgears.filecommands;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.Collection;

/**
 * Stored options for command.
 */
public class OptionsHolder {

    /**
     * Stores options for current command instance.
     */
    private final Options options = new Options();

    /**
     * Adds new option.
     *
     * @param shortName   option short name
     * @param hasArgs     indicates whether option have argument or not
     * @param description description of option
     * @param argsNumber  maximum number of argument values for option
     * @param argName     name for argument
     * @param isRequired  indicates is option required or not
     */
    public void addOption(final String shortName,
                          final boolean hasArgs,
                          final String description,
                          final int argsNumber,
                          final String argName,
                          final boolean isRequired) {
        Option newOption = new Option(shortName, hasArgs, description);
        newOption.setArgs(argsNumber);
        newOption.setArgName(argName);
        newOption.setRequired(isRequired);
        options.addOption(newOption);
    }

    /**
     * Returns current stored options.
     *
     * @return current stored options.
     */
    public Options getOptions() {
        Options newOptions = new Options();
        @SuppressWarnings("unchecked") Collection<Option> oldOptions = options.getOptions();
        for (Option option : oldOptions) {
            newOptions.addOption(option);
        }
        return newOptions;
    }

    /**
     * Returns information of this class as string.
     *
     * @return information of class
     */
    public String getHelp() {
        String answer = "";
        @SuppressWarnings("unchecked") Collection<Option> options = this.options.getOptions();
        for (Option option : options) {
            answer = answer + option.getOpt() + ", option argument name: " + option.getArgName() + ", description: " +
                    option.getDescription() + "\n" + "Option is required: " + option.isRequired() + ".\n\n";
        }

        return answer;
    }
}
