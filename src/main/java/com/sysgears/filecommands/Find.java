package com.sysgears.filecommands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Finds duplicate of sequence in file.
 */
public class Find implements IFileCommand {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(Find.class);

    /**
     * Stores instance of CommandOptions.
     */
    private final OptionsHolder options;

    /**
     * Creates new instance of Find.
     *
     * @param options instance of OptionsHolder
     */
    public Find(final OptionsHolder options) {
        log.debug("Initialise Find");
        log.info("Adding this instance of Find into commands list in CommandHolder");
        this.options = options;
        setOptions();
    }

    /**
     * Sets current options for command.
     */
    private void setOptions() {
        options.addOption("s", true, "Sequence to find in file.", 1, "sequence", true);
        options.addOption("f", true, "Files on what to execute command", 10, "files", true);
    }

    /**
     * Returns name of command.
     *
     * @return name of the command
     */
    @Override
    public String getName() {
        log.info("Starting Find getName method.");
        log.info("Returning - find");
        return "find";
    }

    /**
     * Returns options of command.
     *
     * @return options of command
     */
    @Override
    public Options getOptions() {
        log.info("Starting Find getOptions method.");
        return options.getOptions();
    }


    /**
     * Finds duplicates of sequence in file.
     *
     * @param file on which run command
     * @return message with search result
     * @throws FileNotFoundException if file was not found
     */
    @Override
    public String make(final File file, final CommandLine commandLine) throws FileNotFoundException {
        log.info("Starting make method.");
        int duplicates = 0;
        String answer;
        String sequence = commandLine.getOptionValue("s");
        Scanner scanner = new Scanner(file);
        log.info("Trying to find " + sequence + " in current file.");
        try {
            String line;
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                if (line.split(sequence).length != 0) {
                    duplicates += line.split(sequence).length - 1;
                }
            }
        } finally {
            scanner.close();
        }
        log.info("Result - " + duplicates);
        answer = "In file " + file.getName() + " " + duplicates + " same sequences found.";

        return answer;
    }

    /**
     * Returns information of this class as string.
     *
     * @return information of class
     */
    @Override
    public String getHelp() {
        String answer = "Looks for sequence in files. Returns number of found duplicates. Usage:\n" +
                "find -option -argument\n" + "Options:\n";

        return (answer + options.getHelp());
    }
}
