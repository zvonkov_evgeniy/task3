package com.sysgears.filecommands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;

/**
 * Describes all standard commands for file.
 */
public interface IFileCommand {

    /**
     * Returns name of command.
     *
     * @return name of the command
     */
    public String getName();

    /**
     * Returns options of command.
     *
     * @return options of command
     */
    public Options getOptions();

    /**
     * Runs command and return status message.
     *
     * @param file        on which run command
     * @param commandLine arguments for command
     * @return status message
     * @throws IOException if I\O exception occurs
     */
    public String make(File file, CommandLine commandLine) throws IOException;

    /**
     * Returns current command usage.
     *
     * @return instruction of how to use command
     */
    public String getHelp();
}
