import com.sysgears.filecommands.CommandHolder;
import com.sysgears.filecommands.Find;
import com.sysgears.filecommands.OptionsHolder;
import com.sysgears.filecommands.ReplaceSequence;
import com.sysgears.fileworker.FileWorker;
import com.sysgears.fileworker.LaunchCommand;
import com.sysgears.io.IOController;
import com.sysgears.io.IReaderWriter;
import com.sysgears.service.FileWorkerService;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Main class that initialise all instances of classes for application.
 */
public class Main {

    /**
     * Logger.
     */
    private final static Logger log = Logger.getLogger(Main.class);

    /**
     * Initialise all needed instances for continue.
     *
     * @param args parameters for application
     */
    public static void main(String[] args) {
        try {
            PropertyConfigurator.configure("log4j.properties");
            log.info("Starting main method");
            log.debug("Initialise all commands..");

            // Stores instance of FileWorker.
            final FileWorker fileWorker = new FileWorker();

            // Stores instance of CommandFactory.
            final CommandHolder commandHolder = new CommandHolder();
            log.debug("Adding commands to holder.");
            commandHolder.addCommandToHolder(new Find(new OptionsHolder()));
            commandHolder.addCommandToHolder(new ReplaceSequence(new OptionsHolder(), fileWorker));
            log.debug("All commands saved.");

            // Stores instance of LaunchCommand.
            final LaunchCommand launchCommand = new LaunchCommand(commandHolder, fileWorker);

            // Stores instance of IReaderWriter.
            final IReaderWriter ioSource = new IOController(new PrintStream(System.out),
                    new BufferedInputStream(System.in));

            // Stores instance of FileWorkerService.
            final FileWorkerService fileWorkerService = new FileWorkerService(ioSource, launchCommand, commandHolder);

            fileWorkerService.service();
        } catch (IOException ioe) {
            log.fatal("IOException occurs.", ioe);
        } catch (Throwable t) {
            log.fatal("Cannot execute more.", t);
        }
    }
}
